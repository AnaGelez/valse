![Logo](valse.png)

# Valse

Valse is a MVC framework to create websites with the Vala language. It works
on Windows, Linux, and Mac OS X, with Vala 0.20 minimum.

If you want to help us, please check out [the contribution guide](CONTRIBUTING.md).

# Example of a little webiste

*main.vala* :

```vala
using Valse;

void main () {
    int scgi_port = 9000;
    Router r = new Router (scgi_port);
    r.register ("home", new HomeController ()); // We register a controller
    r.listen (); // we launch the server
}
```

*home-controller.vala* :

```vala
using Valse;

// This is our controller, where all the logic happens
public class HomeController : Controller {
    public HomeController () {
        this.add_action ("index", this.index); // we register an action
    }

    public Response index () {
        return page ("demo.wala"); // in this action, we send the "demo.wala" page
    }
}
```

*demo.wala* :

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Valse sample</title>
    </head>
    <body>
        <h1>Hello, Valse's world!</h1>
    </body>
</html>
```

In a perfect world, you would compile with `valac --pkg valse *.vala`, but Valse can't be installed and integrated with
valac easely yet (it's still an alpha :grin:). So you'll first have to download the sources, then compile them
(see [CONTRIBUTING.md](CONTRIBUTING.md)) and copy the content of the `out` directory and the libscgi files in the folder
that contains the source of your website. Then compile with:

```bash
# Use .dll instead of .so  and .o on Windows
valac *.vala --vapidir=. --pkg valse --pkg libscgi-1.0 --pkg gio-2.0 --pkg gee-0.8 --pkg libsoup-2.4 -X "-I." -X valse.o -X "-I." -X libscgi-1.0.so -o mywebiste
```

Then launch Nginx, or any web server, and make it listening for SimpleCGI (or SCGI) on port 9000.
Run your website's binary (with `LD_LIBRARY_PATH=. ./mywebiste` to make sure that valse will be found), and go on localhost. Enjoy!
