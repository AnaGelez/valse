try {
    rm test/serv.exe -ErrorAction Stop
}
catch {
    echo "serv.exe doesn't exist, we can't delete it."
}

valac --vapidir=. --pkg libgscgi-1.0 -X "-I." -X libgscgi-1.0.dll --pkg libsoup-2.4 --pkg gee-1.0 --pkg sqlite3 valse/*.vala valse/core/*.vala valse/wala/*.vala --library=out/valse -H out/valse.h -X -fPIC -X -shared -o out/valse.dll -X -w


If ($lastexitcode -eq 0) {
    echo " "
    echo "--- Valse build succeded ---"
    echo " "

    cp ./out/valse.dll ./test
    cp ./out/valse.h ./test
    cp ./out/valse.vapi ./test

    ./test/build-test.ps1

} Else {
    echo " "
    echo "--- Valse build failed ---"
    echo " "
}
