using Valse;
using Gee;

/**
* Here begins the tests.
*/
int main (string[] args) {

    Test.init (ref args);

    // Check that all the static files are found
    Test.add_func ("/valse/router/staticfiles", () => {
        Router r = new Router();
        string[] statics = r.get_static_files_list ();
        ArrayList<string> normalRes = new ArrayList<string> ();
        normalRes.add ("static/main.css");
        normalRes.add ("static/img/valse.png");
        normalRes.add ("static/js/main.js");

        if (statics.length != 3) {
            Test.fail ();
            print ("More or less than 3 static files (they are %d): \n", statics.length);
            foreach (string file in statics) {
                print ("\t- %s\n", file);
            }
            return;
        }

        foreach (string file in statics) {
            if(!normalRes.contains(file)) {
                print ("%s shouldn't be here !\n", file);
                Test.fail ();
                return;
            }
        }
    });

    Test.add_func ("/valse/utils/decodeuri", () => {
        string expected = "2 + 2 = 4, hello \"world!\"";
        string encoded = "2+%2B+2+%3D+4%2C+hello+%22world!%22";
        string result = decode_uri (encoded);

        if (result != expected) {
            print ("Expected %s, but got %s\n", expected, result);
            Test.fail ();
        }
    });

    Test.run ();

    return 0;
}
