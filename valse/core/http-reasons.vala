using Gee;

namespace Valse {

    /**
    * Gets the HTTP reason phrase for a given code.
    *
    * @param code A HTTP code
    * @return A string containing an HTTP reason phrase
    */
    public string get_reason (int code) {
        HashMap<int, string> http_reasons = new HashMap<int, string> ();
        http_reasons [100] = "Continue";
        http_reasons [101] = "Switching Protocols";
        http_reasons [200] = "OK";
        http_reasons [201] = "Created";
        http_reasons [202] = "Accepted";
        http_reasons [203] = "Non-Authoritative Information";
        http_reasons [204] = "No Content";
        http_reasons [205] = "Reset Content";
        http_reasons [206] = "Partial Content";
        http_reasons [300] = "Multiple Choices";
        http_reasons [301] = "Moved Permanently";
        http_reasons [302] = "Found";
        http_reasons [303] = "See Other";
        http_reasons [304] = "Not Modified";
        http_reasons [305] = "Use Proxy";
        http_reasons [307] = "Temporary Redirect";
        http_reasons [400] = "Bad Request";
        http_reasons [401] = "Unauthorized";
        http_reasons [402] = "Payment Required";
        http_reasons [404] = "Not Found";
        http_reasons [403] = "Forbidden";
        http_reasons [405] = "Method Not Allowed";
        http_reasons [407] = "Proxy Authentication Required";
        http_reasons [406] = "Not Acceptable";
        http_reasons [408] = "Request Time-out";
        http_reasons [409] = "Conflict";
        http_reasons [410] = "Gone";
        http_reasons [411] = "Length Required";
        http_reasons [412] = "Precondition Failed";
        http_reasons [413] = "Request Entity Too Large";
        http_reasons [414] = "Request-URI Too Large";
        http_reasons [415] = "Unsupported Media Type";
        http_reasons [416] = "Requested range not satisfiable";
        http_reasons [417] = "Expectation Failed";
        http_reasons [500] = "Internal Server Error";
        http_reasons [501] = "Not Implemented";
        http_reasons [502] = "Bad Gateway";
        http_reasons [503] = "Service Unavailable";
        http_reasons [504] = "Gateway Time-out";
        http_reasons [505] = "HTTP Version not supported";
        return http_reasons [code];
    }

}
