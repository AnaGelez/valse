using Gee;

namespace Valse {

    /**
    * A response to sent to the client.
    */
    public class Response : Object {

        /**
        * The response's headers.
        */
        public HashMap<string, string> headers { get; set; default = new HashMap<string, string> (); }

        /**
        * The HTTP code of the response.
        */
        public int error_code { get; set; default = 200; }

        /**
        * The mime-type of the response.
        */
        public string mime_type { get; set; default = "text/html"; }

        /**
        * Is the request raw data (not text).
        */
        public bool is_data { get; set; default = false; }

        /**
        * The data of the request.
        */
        public uint8[] raw_data { get; set; }

        /**
        * The body of the response.
        */
        public string body { get; set; }

        /**
        * The cookies to set.
        */
        public HashMap<string, string> cookies { get; set; default = new HashMap<string, string> ();  }

        /**
        * Create an empty response, that should be modified before sending.
        */
        public Response () {
            this.error_code = 200;
            this.body = "";
        }

        /**
        * Creates a new response from HTML.
        *
        * @param html The HTML used as the body of the new request.
        */
        public Response.from_html (string html) {
            this.body = html;
        }

    }

}
