namespace Valse {

    /**
    * Delegate to store a simple action.
    */
    public delegate Response ActionHandler ();

    /**
    * Delegate to store a complete action.
    */
    public delegate Response CompleteActionHandler (Request req, Response res);

    /**
    * Handles an action.
    */
    public class Action : Object {

        /**
        * Handler for a simple action.
        */
        public ActionHandler handler { get; owned set; }

        /**
        * Handler for a complete action.
        */
        public CompleteActionHandler complete_handler { get; owned set; }

        /**
        * Identifier for the options of a complete action (e.g: /controller/action/option1/option2)
        */
        public string[] options { get; set; }

        /**
        * Create a new simple action, with the given handler.
        *
        * @param hdlr The handler for the new action.
        */
        public Action (owned ActionHandler hdlr) {
            this.handler = (owned) hdlr;
        }

        /**
        * Create a new complete action, with the given handler.
        *
        * @param hdlr The handler for the new action.
        * @param opt The options that the action can handle.
        */
        public Action.complete (owned CompleteActionHandler hdlr, string[] opt) {
            this.complete_handler = (owned) hdlr;
            this.options = opt;
        }

    }

}
