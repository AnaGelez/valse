namespace Valse {

    /**
    * The options of a {@link Valse.Routeur}
    */
    public class RouterOptions : Object {

        /**
        * Specifies if we are in debug mode or not.
        *
        * If we debug, some messages will be shown.
        */
        public bool debug { get; set; default = true; }

        /**
        * The render engine used to render views
        *
        * It will, for example, be used in {@link Valse.Controller.page}
        */
        public RenderEngine render_engine { get; set; default = new Wala (); }

        /**
        * The default address for the DB.
        */
        public string db_url { get; set; default = "database.db"; }

        /**
        * Should we minify HTML ?
        */
        public bool minify { get; set; default = true; }

        /**
        * Are we in dev environment ?
        *
        * If yes, we will use the internal server based on libsoup.
        * If not, we will use SCGI to communicate with Apache, Nginx ...
        */
        public bool dev_env { get; set; default = false; }

        /**
        * How many subporcesses can we start to handle requests ?
        *
        * Or, how many requests can we handle at the same time ?
        */
        public int process_count { get; set; default = 15; }

    }

}
