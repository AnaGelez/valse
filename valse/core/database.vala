using Sqlite;
using Gee;

namespace Valse {

    /**
    * Helper for Sqlite
    */
    public class DataBase : Object {

        /**
        * The Sqlite DB which we interact
        */
        private Database db;

        /**
        * Handle the errors of the database actions
        *
        * @param ec The error code
        * @param message A message to display
        * @return EXIT_SUCCESS if no error was detected, else EXIT_ON_ERROR
        */
        private int handle_sql_errors (int ec, string message = "An error occured") {
            if (ec != Sqlite.OK) {
                log_error ("%s (code %d)".printf (message, db.errcode ()));
                return EXIT_ON_ERROR;
            }
            return EXIT_SUCCESS;
        }

        /**
        * Execute a given SQL query and return the status
        *
        * @param query The SQL query to execute
        * @param callback
        * @return EXIT_SUCCESS if no error was detected, else EXIT_ON_ERROR
        */
        private int execute_query (string query, Sqlite.Callback? callback = null) {
            log_debug ("Executing query: \n%s".printf (query));

            string errmsg = "";
            int ec = db.exec (query, callback, out errmsg);

            if (errmsg == null) {
                errmsg = "An error occured";
            }

            return handle_sql_errors (ec, errmsg);
        }

        /**
        * The constructor of the wrapper
        *
        * @param url The address of the DB
        */
        public DataBase (string url = Router.options.db_url) {
            // We init the Sqlite DB
            int ec = Sqlite.Database.open (url, out this.db);
            string msg = "Can't open database";
            this.handle_sql_errors (ec, msg);
        }

        /**
        * Registers a new model in the DB
        *
        * @param model The {@link GLib.Type} of the model to register
        * @return The name of the created table
        */
        public string register<G> () {
            Type model = typeof (G);
            string query = "CREATE TABLE "; // create a table
            query += model.name () + " ( "; // with the name of the model
            bool first = true; // are we on the first property ?

            foreach (ParamSpec spec in ((ObjectClass) model.class_ref ()).list_properties ()) {
                // for each property
                string type = "NONE";
                string name = spec.name.replace ("-", "_");
                // We define the type of the property in the database
                switch (spec.value_type.name ()) {
                    case "GDateTime":
                    case "gchararray":
                        type = "TEXT";
                        break;
                    case "gint":
                        type = "INT";
                        break;
                    case "gdouble":
                        type = "REAL";
                        break;
                    case "gfloat":
                        type = "REAL";
                        break;
                }

                if (type == "NONE") {
                    continue;
                }
                // We look if we are on the first property
                if (first) {
                    // if so, we don't put a comma before
                    query += "%s %s NOT NULL".printf (name, type);
                    first = false;
                } else {
                    // else, we put one
                    query += ", %s %s NOT NULL".printf (name, type);
                }

            }
            // owe close the query
            query += " );";

            // and we execute it
            execute_query (query);

            // We return the name of the model in the DB, because it can be different as in the code
            return model.name ();
        }

        /**
        * Add an element in the DB.
        *
        * @param obj The element to add.
        * @param table The name of the table where we add the object.
        * @return The ID of the registered object
        */
        public int add (Object obj, string? table = null) {
            // if no table is specified, we find it automatically
            if (table == null) {
                table = obj.get_type ().name ();
            }

            string query = "INSERT INTO ";
            string vals = " ("; // values to insert
            query += table; // we add the name of the table
            query += " (";

            int i = 1; // count the number of properties we
            foreach (ParamSpec spec in obj.get_class ().list_properties ()) {
                // we add quotes if the value is a text
                if (spec.value_type == typeof (string)) {
                    vals += "\"";
                }

                if (spec.value_type == typeof (DateTime)) {
                    // we get the value of the variable
                    query += spec.name.replace ("-", "_");
            		GLib.Value val = GLib.Value (typeof (DateTime));
            		string prop = spec.name.replace ("-", "_");
            		obj.get_property (prop, ref val);
            		vals += "\"" + ((DateTime) val).format ("%F %H-%M-%S.000") + "\"";
                } else if (!spec.value_type.is_a (typeof (Object))) {
                    // we get the value of the variable
                    query += spec.name.replace ("-", "_");
                    GLib.Value val = GLib.Value (typeof (string));
                    string prop = spec.name.replace ("-", "_");
                    obj.get_property (prop, ref val);
                    vals += (string) val;
                } else {
                    // removing ", " at the end
                    query = query[0:query.length - 2];
                    vals = vals[0:vals.length - 2];
                }

                // we close quotes, if needed
                if (spec.value_type == typeof (string)) {
                    vals += "\"";
                }

                // we add a coma, if needed
                if (i < obj.get_class ().list_properties ().length) {
                    query += ", ";
                    vals += ", ";
                } else {
                    query += ") ";
                    vals += ")";
                }

                i++;
            }

            // we add the values, and we end the query
            query += "VALUES";
            query += vals;
            query += ";";

            // we run the query
            execute_query (query);

            // we return the ID of the inserted object
            return (int) db.last_insert_rowid ();
        }

        /**
        * Gives you the element corresponding to an ID.
        *
        * @param id The ID of the element to retrive.
        * @param type The {@link GLib.Type} that corresponds of the table where we look for the element.
        * @return The object corresponding to the ID.
        */
        public Object get_id<G> (int id) {
            Type type = typeof (G);
            // we create the object to return
            Object obj = Object.new (type);
            // we create the query
            string query = "SELECT * FROM ";
            query += type.name ();
            query += " WHERE rowid = %d".printf (id);

            // we run the query
            execute_query (query, (ncol, vals, names) => {
                // we define the properties of the object
                for (int i = 0; i < ncol; i++) {
                    // we choose the good type
                    Type val_type = typeof (string);
                    foreach (ParamSpec spec in ((ObjectClass) type.class_ref ()).list_properties ()) {
                        if (!(spec.value_type.name () in new string[]{ "gint", "gdouble", "gchararray", "GDateTime" })) {
                            continue;
                        }

                        if (spec.name.replace ("-", "_") == names [i]) {
                            val_type = spec.value_type;
                        }
                    }
                    // we set the properties
                    GLib.Value val = GLib.Value (val_type);

                    switch (val.type ().name ()) {
                        case "gchararray":
                            val.set_string (vals [i]);
                            break;
                        case "gint":
                            val.set_int (int.parse (vals [i]));
                            break;
                        case "gdouble":
                            val.set_double (double.parse (vals [i]));
                            break;
                        case "GDateTime":
                            int year = 0, month = 0, day = 0, hour = 0, minute = 0, second = 0;
                            log_debug ("OK 1\n");
                            vals [i].scanf ("%d-%d-%d %d-%d-%d.000", ref year, ref month, ref day, ref hour, ref minute, ref second);
                            log_debug ("OK 2\n");
                            DateTime dt = new DateTime.utc (year, month, day, hour, minute, (double) second);
                            val.set_boxed (dt);
                            break;
                    }

                    obj.set_property (names [i], val);
                }
                return 0;

            });

            // we send the object
            return obj;
        }

        /**
        * Delete the element corresponding to an ID.
        *
        * @param id The ID of the element to delete.
        * @param type The {@link GLib.Type} corresponding to the tble where we look for the element.
        */
        public void delete_id<G> (int id) {
            Type type = typeof (G);
            string query = "DELETE FROM ";
            query += type.name ();
            query += " WHERE rowid = %d;".printf (id);

            execute_query (query);
        }

        /**
        * Gives you all the element of a table
        *
        * @param type The {@link GLib.Type} corresponding to the table where we look for.
        * @return A {@link Gee.ArrayList} contening all the element in the table.
        */
        public ArrayList<Object> all<G> () {
            Type type = typeof (G);
            ArrayList<Object> result = new ArrayList<Object> ();

            // the query to run
            string query = "SELECT * FROM %s;".printf (type.name ());

            execute_query (query, (ncol, vals, names) => {
                string? first_prop = ""; // object's first property's name

                for (int i = 0; i < ncol; i++) {
                    // for each result
                    Type val_type = typeof (string); // the type of the variable to define

                    foreach (ParamSpec spec in ((ObjectClass) type.class_ref ()).list_properties ()) {
                        if (!(spec.value_type.name () in new string[]{ "gint", "gdouble", "gchararray", "GDateTime" })) {
                            continue;
                        }

                        if (first_prop == "") { // we set the name of the first prop, if it isn't already set
                            first_prop = spec.name.replace ("-", "_");
                        }

                        if (spec.name.replace ("-", "_") == names [i]) {
                            val_type = spec.value_type; // we take the type we need
                        }
                    }
                    // if we are on the first property = we are on a new object
                    if (first_prop != "" && names[i] == first_prop) {
                        // we add the old object to the list, and we create a new one
                        result.add (Object.new (type));
                    }

                    // we set the property
                    GLib.Value val = GLib.Value (val_type);

                    switch (val.type ().name ()) {
                        case "gchararray":
                            val.set_string (vals[i]);
                            break;
                        case "gint":
                            val.set_int (int.parse (vals[i]));
                            break;
                        case "gdouble":
                            val.set_double (double.parse (vals[i]));
                            break;
                        case "GDateTime":
                            int year = 0, month = 0, day = 0, hour = 0, minute = 0, second = 0;
                            vals[i].scanf ("%d-%d-%d %d-%d-%d.000", ref year, ref month, ref day, ref hour, ref minute, ref second);
                            DateTime dt = new DateTime.utc (year, month, day, hour, minute, (double) second);
                            val.set_boxed (dt);
                            break;
                    }
                    result[result.size - 1].set_property (names[i].replace ("_", "-"), val);
                }
                return 0;
            });

            return result;
        }

        /**
        * Update the element corresponding to an ID
        *
        * @param id The ID of the object to update
        * @param up_version The updated version of the model
        */
        public void update (int id, Object up_version) {
            string query = "UPDATE ";
            query += up_version.get_type ().name ();
            query += " SET ";

            int i = 1;
            foreach (ParamSpec spec in up_version.get_class ().list_properties ()) {
                if (!(spec.value_type.name () in new string[]{ "gint", "gdouble", "gchararray", "GDateTime" })) {
                    continue;
                }
                // for each property of the object
                query += spec.name.replace ("-", "_");

                query += " = ";

                // we put some quotes if it's a text
                if (spec.value_type == typeof (string)) {
                    query += "\"";
                }

                // we add the value of the property
                GLib.Value val = GLib.Value (typeof (string));
                string prop = spec.name.replace ("-", "_");
                up_version.get_property (prop, ref val);
                query += (string) val;

                // we close the quotes if needed
                if (spec.value_type == typeof (string)) {
                    query += "\"";
                }

                // we add a comma if needed
                if (i < up_version.get_class ().list_properties ().length) {
                    query += ", ";
                } else {
                    query += " ";
                }

                i++;
            }

            // we run the query and we print errors
            query += "WHERE rowid = %d;".printf (id);
            execute_query (query);
        }
    }
}
