using Gee;

namespace Valse {

    /**
    * A request sent by the user.
    */
    public class Request : Object {

        /**
        * The options of the request
        */
        internal string[] options { get; set; }

        /**
        * The path of the request
        */
        public string path { get; set; }

        /**
        * The parameters of the request.
        */
        public HashMap<string, string> query { get; set; default = new HashMap<string, string> (); }

        /**
        * The request's method (GET, POST, PATCH, DELETE, etc).
        */
        public string method { get; private set; default = "GET"; }

        /**
        * The IP adress of the visitor.
        */
        public string ip { get; private set; default = "127.0.0.1"; }

        /**
        * The body of the request.
        */
        public string body { get; set; }

        /**
        * The parameters sent in a POST request and files uploaded.
        */
        public HashMap<string, string> form_data { get; private set; default = new HashMap<string, string> (); }

        /**
        * The ID of the request (e.g: /controller/action/id1/id2)
        */
        public HashMap<string, string> query_ids { get; set; default = new HashMap<string, string> (); }

        /**
        * The HTTP headers of the request
        */
        public HashMap<string, string> headers { get; set; default = new HashMap<string, string> (); }

        /**
        * The cookies stored by the web browser.
        */
        public HashMap<string, string> cookies { get; set; default = new HashMap<string, string> (); }

        /**
        * Creates a new request
        */
        public Request (SCGI.Request base_req) {
            bool is_multipart = false;
            string boundary = "";
            bool is_url_encoded = false;
            int content_length = 0;

            base_req.params.foreach ((entry) => {
                var k = entry.key;
                var v = entry.value;
                if (Regex.match_simple ("^HTTP_", k)) {
                    // http headers
                    string hdr_name = k.replace ("HTTP_", "");
                    hdr_name = hdr_name.replace ("_", "-");
                    string res = "";
                    bool cap = true;
                    for (int i = 0; i < hdr_name.length; i++) {
                		if (hdr_name.valid_char (i)) {
                            if (cap) {
                                res += hdr_name.get_char (i).to_string ().up ();
                                cap = false;
                            } else {
                                if (hdr_name.get_char (i) == '-') {
                                    cap = true;
                                }
                                res += hdr_name.get_char (i).to_string ().down ();
                            }
                		}
                	}
                    this.headers [res] = v;

                    if (res == "Cookie") {
                        foreach (string cookie in v.split (";")) {
                            cookie = cookie.strip ();
                            this.cookies [cookie.split ("=")[0]] = cookie.split ("=")[1];
                        }
                    }
                } else if (k == "REQUEST_METHOD"){
                    // request's method (GET, POST ...)
                    this.method = v;
                } else if (k == "REMOTE_ADDR") {
                    this.ip = v;
                } else if (k == "QUERY_STRING") {
                    foreach (string param in v.split ("&")) {
                        this.query [param.split ("=") [0]] = param.split ("=") [1];
                    }
                } else if (k == "DOCUMENT_URI") {
                    this.path = v;
                } else if (k == "CONTENT_TYPE") {
                    try {
                        print ("content type : '%s'\n", v);
                        var multipart_re = new Regex ("multipart\\/form-data; boundary=(?<boundary>.*)");
                        MatchInfo info;
                        if (multipart_re.match (v, 0, out info)) {
                            boundary = info.fetch_named ("boundary").replace ("\r\n", "");
                            is_multipart = true;
                        } else if (v == "application/x-www-form-urlencoded") {
                            is_url_encoded = true;
                        }
                    } catch (Error err) {
                        log_error (err.message);
                    }
                } else if (k == "CONTENT_LENGTH") {
                    content_length = int.parse (v);
                }
                return true;
            });

            if (is_multipart) {
                try {
                    DataInputStream dis = (DataInputStream) base_req.input;
                    string line;
                    string name = "";
                    string val = "";
                    while ((line = dis.read_line (null)) != null) {
                        line = line.replace ("\r", "");
                        line = line.replace ("\n", "");
                        if (line == "--" + boundary + "--") {
                            if (name != "" && val != "") {
                                this.form_data [name] = val;
                            }
                            name = "";
                            val = "";
                            break;
                        }

                        if (line == "--" + boundary) {
                            if (name != "" && val != "") {
                                this.form_data [name] = val;
                            }
                            name = "";
                            val = "";
                        }

                        if (name != "" && line.length > 0) {
                            if (val == "") {
                                val = line;
                            } else {
                                val += "\n" + line;
                            }
                        }

                        Regex name_re = new Regex ("^Content-Disposition: form-data; name=\"(?<name>.*)\"");
                        MatchInfo name_info;
                        if (name_re.match (line, 0, out name_info)) {
                            name = name_info.fetch_named ("name");
                        }
                    }
                } catch (Error err) {
                    log_error (err.message);
                }
            } else if (is_url_encoded) {
                try {
                    DataInputStream dis = (DataInputStream) base_req.input;
                    uint8[] data = {};
                    uint8 d;
                    for (int i = 0; i < content_length; i++) {
                        data += dis.read_byte ();
                    }
                    string line = (string) data;
                    line = line.replace ("\r", "");
                    line = line.replace ("\n", "");
                    foreach (string pair in line.split("&")) {
                        if (pair.split ("=").length != 2) {
                            assert_not_reached ();
                        }
                        var k = decode_uri (pair.split ("=")[0]);
                        var v = decode_uri (pair.split ("=")[1]);
                        this.form_data[k] = v;
                    }
                } catch (Error err) {
                    log_error (err.message);
                }
            }
        }

        internal void set_options_names (string[] opts_names) {
            int i = 0;
            foreach (string opt_name in opts_names) {
                this.query_ids [opt_name] = this.options [i];
                i++;
            }
        }

    }

}
