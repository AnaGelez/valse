using Gee;
using Valse.Default;

namespace Valse {

    /**
    * The base of any controller.
    *
    * Provides methods like {@link Valse.Controller.page} or
    * {@link Valse.Controller.db}.
    */
    public class Controller : Object {

        /**
         * The actions registered for this controller
         */
        public HashMap<string, Action> actions { get; set; default = new HashMap<string, Action> (); }

        /*
        * The error controller to use with this controller
        */
        public ErrorController? error_controller { get; set; default = null; }

        /**
        * Redirects to a given URL.
        *
        * @param url The URL on which to redirect.
        * @param permanent Tells to the client if it whould always do this redirection without asking anymore. Defaults to false.
        */
        public Response redirection (string url, bool permanent = false) {
            Response res = new Response ();
            res.headers ["Location"] = url;
            if (permanent) {
                res.error_code = 301;
            } else {
                res.error_code = 302;
            }
            return res;
        }

        /**
         * Renders a page using the default render engine.
         *
         * @param file The path of the file containing the view to render
         * @param obj_model The model for the view that will be rendered
         * @param tags The tags for this view
         * @return A {@link Valse.Response}, ready to be sent
         */
        public Response page (string file, HashMap<string, Object> obj_models = new HashMap<string, Object> (), string[] tags = {}) {
            return this.template (get_contents (file), obj_models, tags);
        }

        /**
        * Renders a template using the default render engine.
        *
        * @param html The html of the page
        * @param obj_model The model for the view that will be rendered
        * @param tags The tags for this view
        * @return A {@link Valse.Response}, ready to be sent
        */
        public Response template (string html, HashMap<string, Object> obj_models = new HashMap<string, Object> (), string[] tags = {}) {
            // on crée la vue
            View view = new View ();
            view.content = html;

            foreach (var entry in obj_models.entries) {
                view.models [entry.key] = create_model (entry.value);
            }
            view.tags = tags;

            // On renvoie l'interprétation de la vue
            return new Response.from_html (Router.options.render_engine.render_page (view));
        }

        /**
        * Displays an error page
        *
        * @param code The HTTP error code
        * @param message A description of the error
        */
        public Response error (int code, string? message = null) {
            return this.error_controller.error (code, message == null ? get_reason (code) : message);
        }

        /**
        * Creates a model.
        *
        * Creates a model from keys and values.
        * @param key1
        */
        public HashMap<string, Object> model (string key1, Object obj1, ...) {
            va_list others = va_list();
            HashMap<string, Object> res = new HashMap<string, Object> ();
            res[key1] = obj1;

            while (true) {
                string? k = others.arg<string> ();
                if (k == null) {
                    break;
                }

                Object? v = others.arg<Object> ();
                if (v == null) {
                    log_warning ("[WARNING] Using the model method with a key (%s), but no associated value.\n".printf (k));
                    break;
                }

                res[k] = v;
            }

            return res;
        }

        /**
        * Shortcut to access the {@link Valse.DataBase} class.
        */
        public DataBase db {
            owned get {
                return new DataBase ();
            }
        }

        /**
        * Reads the content of a file.
        *
        * @param path The path of the file to read
        * @return The files content
        */
        private string get_contents (string path) {
            string result = "";

            var file = File.new_for_path (path);
            if (!file.query_exists ()) {
                return Errors.not_found (path).body;
            }

            try {
                var dis = new DataInputStream (file.read ());
                string line;
                while ((line = dis.read_line (null)) != null) {
                    result += line + "\n";
                }
            } catch (Error e) {
                log_error (e.message);
            }

            return result;
        }

        /**
        * Generate a model that could be used in a view
        *
        * @param obj The object that stands for the model
        * @return A {@link Gee.HashMap} usable in a view
        */
        private HashMap<string, string> create_model (Object obj) {
            // special behavior for Many
            if (obj.get_type ().is_a (typeof (Many))) {
                return ((Many) obj).to_hashmap ();
            }

            // for standard GObject objects
            HashMap<string, string> result = new HashMap<string, string> ();

            foreach (ParamSpec spec in obj.get_class ().list_properties ()) {
                // for each property
                if (spec.value_type == typeof (Many)) {
                    GLib.Value val = GLib.Value (typeof (Many)); // to get variable's value
                    obj.get_property (spec.get_nick (), ref val);
                    var values = ((Many) val.get_object ()).to_hashmap ().entries;
                    foreach (var entry in values) {
                        result ["." + spec.get_nick () + entry.key] = entry.value;
                    }
                } else if (spec.value_type == typeof (DateTime)) {
                    GLib.Value val = GLib.Value (typeof (DateTime));
            		string prop = spec.name.replace ("-", "_");
            		obj.get_property (spec.name, ref val);
                    result ["." + prop] = ((DateTime) val).format ("%F %H-%M-%S.000");
                } else {
                    GLib.Value val = GLib.Value (typeof (string)); // to get variable's value
                    obj.get_property (spec.get_nick (), ref val); // we get this value
                    result ["." + spec.get_nick ().replace ("-", "_")] = (string) val; //we set it in our model (used in the view)
                }
            }

            return result;
        }

        /**
        * Registers a simple action.
        *
        * @param name The name of the action, his route.
        * @param act The handler for this action.
        */
        public void add_action (string name, owned ActionHandler act) {
            actions [name] = new Action ((owned) act);
        }

        /**
        * Registers a complete action.
        *
        * @param name The name of the action, his route.
        * @param act The handler for this action.
        * @param options The options of this action.
        */
        public void add_complete_action (string name, owned CompleteActionHandler act, string[] options = {}) {
            actions [name] = new Action.complete ((owned) act, options);
        }

        /**
        * Run an action of this controller.
        *
        * @param name The name of the action to run.
        * @param msg The {@link Soup.Message} to give to the action, if it's complete.
        * @param query The {@link Gee.HashTable} to give to the action, if it's complete.
        * @param client The {@link Soup.ClientContext} to give to the action, if it's complete.
        */
        public Response run_action (string name, Request req) {
            // we look for an action named "name"
            if (this.actions.has_key (name)) {
                if (this.actions [name].handler != null) {
                    // if it's a simple action
                    return this.actions [name].handler ();
                } else if (this.actions [name].complete_handler != null) {
                    // if it's a complete action
                    var opts = this.actions [name].options;
                    req.set_options_names (opts);
                    return this.actions [name].complete_handler (req, new Response.from_html (""));
                }
            }
            // if we don't find the action, we send an error.
            return Errors.not_found (name);
        }
    }
}
