namespace Valse.Default {
    /*
    * The default error controller.
    *
    * Use a child class to define custom error pages.
    */
    public class ErrorController : Controller {

        protected string? error_message { get; set; }

        public virtual Response error (int code, string message) {
            if (this.actions.has_key (code.to_string ()) && this.actions[code.to_string ()].handler != null) {
                this.error_message = message;
                var res = this.actions[code.to_string ()].handler ();
                this.error_message = null;
                return res;
            } else {
                return new Response.from_html ("<h1>%d: %s</h1>".printf (code, message));
            }
        }
    }
}
