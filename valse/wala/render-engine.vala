namespace Valse {

    /**
    * Interface describing a render engine
    */
    public interface RenderEngine : Object {

        /**
        * The name of the render engine
        */
        public abstract string name { get; set; }

        /**
        * The method that's used to do the render of a view
        */
        public abstract string render_page (View view);

    }
}
