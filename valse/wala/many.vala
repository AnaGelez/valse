using Gee;

namespace Valse {

    public class Many<G> : ArrayList<G> {

        public Many (EqualDataFunc<G>? eq_func = null) {
            base (eq_func);
        }

        public Many.wrap (owned G[] base_arr, EqualDataFunc<G>? eq_func = null) {
            base.wrap (base_arr, eq_func);
        }

        public HashMap<string, string> to_hashmap () {
            HashMap<string, string> result = new HashMap<string, string> ();
            Type t = typeof (G);

            int index = 0;
            foreach (G item in this) {
                string key = "[%d]".printf (index);
                if (t.is_a (typeof (Object))) {
                    Object obj = (Object) item;
                    foreach (var spec in obj.get_class ().list_properties ()) {
                        if (spec.value_type == typeof (string)) {
                            Value val = Value (typeof (string));
                            obj.get_property (spec.get_nick (), ref val);
                            result [key + "." + spec.get_nick ()] = (string) val;
                        } else if (spec.value_type == typeof (int)) {
                            Value val = Value (typeof (int));
                            obj.get_property (spec.get_nick (), ref val);
                            result [key + "." + spec.get_nick ()] = ((int) val).to_string ();
                        }
                    }
                } else if (t == typeof (string)) {
                    result [key] = (string) item;
                } else if (t == typeof (int)) {
                    result [key] = ((int) item).to_string ();
                }
                index++;
            }
            result [".count"] = index.to_string ();

            return result;
        }

    }

/*


    public class Many<G> : Object {

        private G[] _arr {get; set; default = new G[] {};}

        public Many () {

        }

        public Many.from_array (G[] from) {
            this._arr = from;

        }

        public Many.from_arraylist (ArrayList<G> from) {
            foreach (G item in from) {
                this._arr [this._arr.length] = item;
            }
        }

        public new G @get (int index) {
            assert (index < this._arr.length);
            return this._arr [index];
        }

        public void add (G item) {
            this._arr [this._arr.length] = item;
        }

        public new void @set (int index, G item) {
            this._arr [index] = item;
        }

        public HashMap<string, string> to_hashmap () {

            HashMap<string, string> result = new HashMap<string, string> ();
            Type t = typeof (G);

            int index = 0;
            foreach (G item in this._arr) {
                string key = "[%d]".printf (index);
                if (t.is_a (typeof (Object))) {
                    Object obj = (Object) item;
                    foreach (var spec in obj.get_class ().list_properties ()) {
                        if (spec.value_type == typeof (string)) {
                            Value val = Value (typeof (string));
                            obj.get_property (spec.get_nick (), ref val);
                            result [key + "." + spec.get_nick ()] = (string) val;
                        } else if (spec.value_type == typeof (int)) {
                            Value val = Value (typeof (int));
                            obj.get_property (spec.get_nick (), ref val);
                            result [key + "." + spec.get_nick ()] = ((int) val).to_string ();
                        }
                    }
                } else if (t == typeof (string)) {
                    result [key] = (string) item;
                } else if (t == typeof (int)) {
                    result [key] = ((int) item).to_string ();
                }
                index++;
            }
            result [".count"] = index.to_string ();

            return result;
        }

    }*/
}
