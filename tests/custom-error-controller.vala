using Valse;
using Valse.Default;

public class CustomErrorController : ErrorController {

    public override Response error (int code, string message) {
        return new Response.from_html ("<h1>:(</h1><p>Something went wrong: %s</p><p>(Error code: %d)</p><h2><a href=\"/\">Go back home</a></h2>".printf (message, code)) {
            error_code = code
        };
    }
}
