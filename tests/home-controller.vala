using Valse;

/**
* Controller on /
*/
public class HomeController : Controller {
    public HomeController () {
        this.add_action ("index", this.index);
        this.add_action ("truc", this.truc);
        this.add_action ("wala", this.wala);
        this.add_action ("oupa", this.deleted);
        this.add_complete_action ("cookies", this.cookies);
        this.error_controller = new CustomErrorController ();
    }

    /**
    * The home page.
    *
    * Accessible at /
    */
    public Response index () {
        return page ("views/home.html");
    }

    /**
    * A page to test the router.
    *
    * Accessible at /truc
    */
    public Response truc () {
        return page ("views/truc.html");
    }

    /**
    * Wala-related tests
    *
    * Accessible at /wala
    */
    public Response wala () {
        // KTM
        return page ("views/wala.wala", model("page", new Page () {
            title = "Valse KTM"
        },
        "site", new Site () {
            name = "With the model method !"
        }));
    }

    /**
    * Used to test custom error pages
    */
    public Response deleted () {
        return error (404, "The page you are looking for doesn't exist anymore.");
    }

    /**
    * Used for testing cookies
    */
    public Response cookies (Request req, Response response) {
        Response res = new Response ();
        if (req.cookies["test"] != null) {
            res = template ("<h1>Cookie is set !</h1><p>It's the %s nd time you visit this page".printf (req.cookies["test"]));
            res.cookies["test"] = (int.parse (req.cookies["test"]) + 1).to_string ();
        } else {
            res = template ("<h1>Try to refresh...</h1>");
            res.cookies["test"] = "2";
        }
        return res;
    }
}

/**
* A model used in KillTheMod tests.
*/
public class Page : Object {
    public string title { get; set; }
}

/**
* Anoher model used in KillTheMod tests.
*/
public class Site : Object {
    public string name { get; set; }
}
