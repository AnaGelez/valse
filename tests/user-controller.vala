using Valse;
using Valse.Errors;
using Gee;

/*
* User-related actions (create, view, list, etc).
*
* Useful to test Wala and DataBase.
*/
public class UserController : Controller {

    /*
    * Setup of the controller.
    */
    public UserController () {
        this.db.register<User> ();

        this.add_complete_action ("index", list);
        this.add_complete_action ("list", list);

        this.add_complete_action ("delete", delete, { "id" });

        this.add_complete_action ("create", create);
        this.add_complete_action ("register", create);

        this.add_complete_action ("view", view, { "id" });
        this.add_complete_action ("profile", view, { "id" });

        // TODO : update
    }

    /*
    * List all the registred users.
    */
    public Response list (Request req, Response res) {

        User[] base_arr = new User[] {};

        foreach (var user in this.db.all<User> ()) {
            var usr = (User) user;
            base_arr += usr;
        }

        Many<User> usrs = new Many<User>.wrap (base_arr);

        return page ("views/user/list.wala", model ("users", usrs));
    }

    /*
    * Delete an user
    *
    * Takes an option: the ID of the user to delete, else it will send an error.
    */
    public Response delete (Request req, Response res) {
        if (req.query_ids.size == 0) {
            return server_error ("You should give an ID.");
        }

        int id = int.parse (req.query_ids ["id"]);

        this.db.delete_id<User> (id);

        return list (req, res);
    }

    /*
    * Creates a new user.
    *
    * Useful to test forms.
    */
    public Response create (Request req, Response res) {
        if (req.method == "POST") {
            User usr = new User ();
            usr.pseudo = req.form_data ["name"];
            usr.sign = req.form_data ["sign"];
            usr.avatar = req.form_data ["avatar"];
            usr.website = req.form_data ["site"];
            usr.register_date = new DateTime.now_utc ();
            int id = this.db.add (usr);

            return redirection ("/user/profile/%d".printf (id));
        } else {
            return page ("views/user/register.wala");
        }
    }

    /*
    * Shows the profile of an user.
    */
    public Response view (Request req, Response res) {
        if (req.query_ids.size == 0) {
            return list (req, res);
        } else {
            int id = int.parse (req.query_ids["id"]);
            User usr = (User) db.get_id<User> (id);
            usr.posts.add ("Hello, i'm #newhere.");
            usr.posts.add ("What a fantastic website!");
            usr.posts.add ("ping @Plop : do you want a banana?");
            return page ("views/user/profile.wala", model ("mod", usr, "test", new TestModel ("Yet another link", "https://bat41.gitlab.io")));
        }
    }

}
