using Valse;

/*
* We setup our test server here.
*
* It contains manual tests.
*/
void main (string[] args) {
    Valse.log_level = DebugLevel.DEBUG;
    Router r = new Router (9000);
    Router.options.process_count = 150;
    r.register ("user", new UserController ());
    r.register ("home", new HomeController ());
    r.listen ();
}
