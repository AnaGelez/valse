cp libgscgi-1.0.dll ./test
cp libgscgi-1.0.vapi ./test
cp libgscgi-1.0.h ./test

cd test

valac *.vala models/*.vala --vapidir=. --pkg valse --pkg libgscgi-1.0 --pkg gio-2.0 --pkg gee-1.0 --pkg libsoup-2.4 -X "-I." -X valse.dll -X "-I." -X libgscgi-1.0.dll -o serv.exe

If ($lastexitcode -eq 0) {
    echo " "
    echo "--- Server is starting ---"
    echo " "
    start "http://localhost/user/create"
    ./serv
} Else {
    echo " "
    echo "--- Server build failed ---"
    echo " "
}
